#ifndef FFMPEG_HEADER_GUARD 
#define FFMPEG_HEADER_GUARD

// extern "C"
// {
// #include <libavformat/avformat.h>
// #include <libavcodec/avcodec.h>
// #include<libavutil/motion_vector.h>
// }

#include "avcodec.h"
#include "avformat.h"
#include "channel_layout.h"
#include "mathematics.h"
#include "motion_vector.h"

#endif
