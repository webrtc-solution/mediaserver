{
    "dtlsCertificateFile": "/var/tmp/key/certificate.crt",
    "dtlsPrivateKeyFile": "/var/tmp/key/private_key.pem",
    "listenIps": [
        {
            "announcedIp": "52.5.6.245",
            "ip": "0.0.0.0"
        }
    ],
    "logLevel": "info",
    "logTags": [
        "info"
    ],
    "potentialID": "729795",
    "rtcMaxPort": 12560,
    "rtcMinPort": 11501,
    "rtsp": {
        "0": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "1": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "10": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "11": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "12": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "13": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "14": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "2": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "20": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "21": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "22": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "229795F1234CAM14567": {
            "rtsp": "rtsp://root:60056006@10.86.8.16:559/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        },
        "3": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "33": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "34": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "4": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "40": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "41": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "42": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "429795F1234CAM14561": {
            "rtsp": "rtsp://root:0011J00001MUvfQQAT@10.86.0.24:554/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        },
        "43": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "44": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "50": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "51": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "52": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "529795F1234CAM14562": {
            "rtsp": "rtsp://root:001o000000vmbHtAAI@10.86.12.100:558/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        },
        "53": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "54": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "60": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "61": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "62": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "629795F1234CAM14569": {
            "rtsp": "rtsp://root:001o000000vmbHtAAI@10.86.12.100:554/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        },
        "63": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "64": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "70": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "71": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "72": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "729795F1234CAM14528": {
            "rtsp": "rtsp://root:60056005@10.76.0.16:555/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        },
        "73": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "74": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "80": {
            "rtsp": "rtsp://localhost/test.264",
            "state": "streaming"
        },
        "81": {
            "rtsp": "rtsp://localhost/test1.264",
            "state": "streaming"
        },
        "82": {
            "rtsp": "rtsp://localhost/test2.264",
            "state": "streaming"
        },
        "83": {
            "rtsp": "rtsp://localhost/test3.264",
            "state": "streaming"
        },
        "84": {
            "rtsp": "rtsp://localhost/test4.264"
        },
        "929795F1234CAM14569": {
            "rtsp": "rtsp://root:60056006@10.86.8.16:560/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
        }
    },
    "stream_type": "webrtc"
}