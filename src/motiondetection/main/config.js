{
  "dtlsCertificateFile": "/var/tmp/key/certificate.crt",
  "dtlsPrivateKeyFile": "/var/tmp/key/private_key.pem",
  "storage": "/media/pvi-storage/",
  "listenIps": [
    {
      "announcedIp": "52.5.6.245",
      "ip": "0.0.0.0"
    }
  ],
  "logLevel": "info",
  "logTags": [
    "info"
  ],
  "potentialID": "729795",
  "rtcMaxPort": 12560,
  "rtcMinPort": 11501,
  "pre_clip": 5,
  "cam_reconnect": 0,
  "rtsp": {
    "Cam04": {
      "rtsp": "rtsp://admin:247Supp0rt@192.168.1.183:554/RTSP2HLS/media.smp",
      "CLIPSIZE": 20,
      "contourArea":400,
      "md": true,
      "masking_coords" : [ [{"x" : 0,"y" : 0}, {"x" :1280,"y" : 0}, {"x" : 1280, "y" : 720}, {"x" : 0,"y" :720}]]
    },
    "Cam12": {
      "rtsp": "rtsp://root:60056005@192.168.1.190:554/axis-media/media.amp?videocodec=h264&resolution=1280x720&fps=25",
      "CLIPSIZE": 20,
      "DEFAULT_WINDOW_SIZE": 3,
      "DEFAULT_OCCUPANCY_THRESHOLD": 2,
      "DEFAULT_LOCAL_OCCUPANCY_AVG_THRESHOLD": 0.4,
      "DEFAULT_OCCUPANCY_AVG_THRESHOLD": 0.4,
      "contourArea":400,
      "md": true
    }
  },
  "stream_type": "mdserver"
}
